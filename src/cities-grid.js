import {
  database,
} from './helpers/database.js';

const getFirstCityWeather = (data) => {
  const unitsSign = database.getCurrentUnitsSign();
  const currentCityName = data.name;
  const currentCityCountryName = data.sys.country;
  const currentCityTemp = Math.round(data.main.temp);
  const currentCityWeatherType = data.weather[0].description.split(' ').map((word) => {
    return (word.charAt(0).toUpperCase() + word.slice(1));
  }).join(' ');
  const currentCityWeatherIcon = data.weather[0].icon;
  $('#firstLocation').html(`
  <h2>${currentCityName}, ${currentCityCountryName}</h2>
  <div class="row margin-top-left">
  <div class="col no-padding mb-3">
  <image src = ./src/images/icons/second/${currentCityWeatherIcon}.png></image>
  </div>
  <div class="col-6 mr-2 no-padding">
  <h2 class="firstLocTemp">${currentCityTemp}${unitsSign}</h2>
  </div>
  </div>
  <div class="mb-3">
  <h8 class="firstLocTemp">${currentCityWeatherType}</h8>
  </div>
  `).css('background-image', `url("./src/images/backgrounds/${currentCityWeatherIcon}e.png")`).css('background-size', 'cover')
      .addClass(`forecast-border`);
  return data;
};

const getSecondCityWeather = (data) => {
  const unitsSign = database.getCurrentUnitsSign();
  const currentCityName = data.name;
  const currentCityCountryName = data.sys.country;
  const currentCityTemp = Math.round(data.main.temp);
  const currentCityWeatherType = data.weather[0].description.split(' ').map((word) => {
    return (word.charAt(0).toUpperCase() + word.slice(1));
  }).join(' ');
  const currentCityWeatherIcon = data.weather[0].icon;
  $('#secondLocation').html(`
  <h2>${currentCityName}, ${currentCityCountryName}</h2>
  <div class="row margin-top-left">
  <div class="col no-padding mb-3">
  <image src = ./src/images/icons/second/${currentCityWeatherIcon}.png></image>
  </div>
  <div class="col-6 mr-2 no-padding">
  <h2 class="secondLocTemp">${currentCityTemp}${unitsSign}</h2>
  </div>
  </div>
  <div class="mb-3">
  <h8 class="secondLocTemp">${currentCityWeatherType}</h8>
  </div>
  `).css('background-image', `url("./src/images/backgrounds/${currentCityWeatherIcon}e.png")`).css('background-size', 'cover')
      .addClass(`forecast-border`);
  return data;
};

const getThirdCityWeather = (data) => {
  const unitsSign = database.getCurrentUnitsSign();
  const currentCityName = data.name;
  const currentCityCountryName = data.sys.country;
  const currentCityTemp = Math.round(data.main.temp);
  const currentCityWeatherType = data.weather[0].description.split(' ').map((word) => {
    return (word.charAt(0).toUpperCase() + word.slice(1));
  }).join(' ');
  const currentCityWeatherIcon = data.weather[0].icon;
  $('#thirdLocation').html(`
  <h2>${currentCityName}, ${currentCityCountryName}</h2>
  <div class="row margin-top-left">
  <div class="col no-padding mb-3">
  <image src = ./src/images/icons/second/${currentCityWeatherIcon}.png></image>
  </div>
  <div class="col-6 mr-2 no-padding">
  <h2 class="thirdLocTemp">${currentCityTemp}${unitsSign}</h2>
  </div>
  </div>
  <div class="mb-3">
  <h8 class="thirdLocTemp">${currentCityWeatherType}</h8>
  </div>
  `).css('background-image', `url("./src/images/backgrounds/${currentCityWeatherIcon}e.png")`).css('background-size', 'cover')
      .addClass(`forecast-border`);
  return data;
};

const getFourthCityWeather = (data) => {
  const unitsSign = database.getCurrentUnitsSign();
  const currentCityName = data.name;
  const currentCityCountryName = data.sys.country;
  const currentCityTemp = Math.round(data.main.temp);
  const currentCityWeatherType = data.weather[0].description.split(' ').map((word) => {
    return (word.charAt(0).toUpperCase() + word.slice(1));
  }).join(' ');
  const currentCityWeatherIcon = data.weather[0].icon;
  $('#fourthLocation').html(`
  <h2>${currentCityName}, ${currentCityCountryName}</h2>
  <div class="row margin-top-left">
  <div class="col no-padding mb-3">
  <image src = ./src/images/icons/second/${currentCityWeatherIcon}.png></image>
  </div>
  <div class="col-6 mr-2 no-padding mb-3">
  <h2 class="fourthLocTemp">${currentCityTemp}${unitsSign}</h2>
  </div>
  </div>
  <div class="mb-3">
  <h8 class="fourthLocTemp">${currentCityWeatherType}</h8>
  </div>
  `).css('background-image', `url("./src/images/backgrounds/${currentCityWeatherIcon}e.png")`).css('background-size', 'cover')
      .addClass(`forecast-border`);
  return data;
};

const getFifthCityWeather = (data) => {
  const unitsSign = database.getCurrentUnitsSign();
  const currentCityName = data.name;
  const currentCityCountryName = data.sys.country;
  const currentCityTemp = Math.round(data.main.temp);
  const currentCityWeatherType = data.weather[0].description.split(' ').map((word) => {
    return (word.charAt(0).toUpperCase() + word.slice(1));
  }).join(' ');
  const currentCityWeatherIcon = data.weather[0].icon;
  $('#fifthLocation').html(`
  <h2>${currentCityName}, ${currentCityCountryName}</h2>
  <div class="row margin-top-left">
  <div class="col no-padding mb-3">
  <image src = ./src/images/icons/second/${currentCityWeatherIcon}.png></image>
  </div>
  <div class="col-6 mr-2 no-padding mb-3">
  <h2 class="fourthLocTemp">${currentCityTemp}${unitsSign}</h2>
  </div>
  <div>
  </div>
  </div>
  <div class="mb-3">
  <h8 class="fourthLocTemp">${currentCityWeatherType}</h8>
  </div>
  `).css('background-image', `url("./src/images/backgrounds/${currentCityWeatherIcon}e.png")`).css('background-size', 'cover')
      .addClass(`forecast-border`);
  return data;
};

const renderFirst = (cityName) => {
  database.getCurrentWeatherByName(cityName)
      .then(getFirstCityWeather);
};

const renderSecond = (cityName) => {
  database.getCurrentWeatherByName(cityName)
      .then(getSecondCityWeather);
};

const renderThird = (cityName) => {
  database.getCurrentWeatherByName(cityName)
      .then(getThirdCityWeather);
};

const renderFourth = (cityName) => {
  database.getCurrentWeatherByName(cityName)
      .then(getFourthCityWeather);
};

const renderFifth = (cityName) => {
  database.getCurrentWeatherByName(cityName)
      .then(getFifthCityWeather);
};

export const citiesGrid = {
  renderFirst,
  renderSecond,
  renderThird,
  renderFourth,
  renderFifth,
};
