import { config } from './config.js';

const baseUrl = 'http://api.openweathermap.org/data/2.5/';

let units = 'metric';
let unitsSign = '°C';
let windUnitsSign = 'm/s';

const getWeatherByName = (cityName) => {
  return $.get(`${baseUrl}forecast?q=${cityName}&appid=${config.keys.weatherApiKey}&units=${units}`);
};

const getCurrentWeatherByName = (cityName) => {
  return $.get(`${baseUrl}weather?q=${cityName}&appid=${config.keys.weatherApiKey}&units=${units}`);
};

const getCurrentUnitsSign = () => {
  const currentUnitsSign = unitsSign;
  return currentUnitsSign;
};

const getCurrentWindUnitsSign = () => {
  const currentWindUnitsSign = windUnitsSign;
  return currentWindUnitsSign;
};

let latitude;
let longitude;

const getCurrentPosition = () => {
  let currentPosition;
  const getCurrentLocation = (position) => {
    currentPosition = position;
    latitude = currentPosition.coords.latitude;
    longitude = currentPosition.coords.longitude;
  };
  navigator.geolocation.getCurrentPosition(getCurrentLocation);
};

const getCurrentLocationWeather = () => {
  return $.get(`${baseUrl}weather?lat=${latitude}&lon=${longitude}&appid=${config.keys.weatherApiKey}&units=${units}`);
};

const getCurrentLocationForecast = () => {
  return $.get(`${baseUrl}forecast?lat=${latitude}&lon=${longitude}&appid=${config.keys.weatherApiKey}&units=${units}`);
};

const unitsChange = () => {
  let currentCity;
  if ($('#current-city').text()) {
    currentCity = $('#current-city').text();
  }
  if (units === 'metric') {
    units = 'imperial';
    unitsSign = '°F';
    windUnitsSign = 'm/h';
  } else {
    units = 'metric';
    unitsSign = '°C';
    windUnitsSign = 'm/s';
  }
  return currentCity;
};

export const database = {
  getWeatherByName,
  getCurrentWeatherByName,
  getCurrentPosition,
  getCurrentLocationWeather,
  getCurrentLocationForecast,
  unitsChange,
  getCurrentUnitsSign,
  getCurrentWindUnitsSign,
};
