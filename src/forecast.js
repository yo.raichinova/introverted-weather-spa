/* eslint-disable no-undef */
import { database } from './helpers/database.js';
import { forecastData } from './helpers/forecast-api-data.js';

const dayOfTheWeek = (dayOfTheWeekFinder) => {
  const dayOfTheWeekStringObjects = [{
    id: 'Mon',
    val: 'Monday',
  }, {
    id: 'Tue',
    val: 'Tuesday',
  }, {
    id: 'Wed',
    val: 'Wednesday',
  }, {
    id: 'Thu',
    val: 'Thursday',
  }, {
    id: 'Fri',
    val: 'Friday',
  }, {
    id: 'Sat',
    val: 'Saturday',
  }, {
    id: 'Sun',
    val: 'Sunday',
  }];
  let dayOfTheWeekString;
  dayOfTheWeekStringObjects.find((value) => {
    if (value.id === dayOfTheWeekFinder) {
      dayOfTheWeekString = value.val;
    }
  });
  return dayOfTheWeekString;
};

const displayForecastForFirstDay = (data) => {
  const unitsSign = database.getCurrentUnitsSign();
  const maxTemp = data.firstMaxTemp;
  const minTemp = data.firstMinTemp;
  const dayOfTheWeekAbreviation = data.firstDayOfTheWeekAbrev;
  const dayOfTheWeekString = dayOfTheWeek(dayOfTheWeekAbreviation);
  const nextWeatherMain = data.nextWeatherMain;
  const nextWeatherIcon = data.nextWeatherIcon;
  $('#firstDay').css('background-image', `url("./src/images/backgrounds/${nextWeatherIcon}e.png")`).css('background-size', 'cover')
      .addClass('forecast-border')
      .html(`
  <div class="row mb-3 margin-top-left">
  <div class="col no-padding">
  <image src = ./src/images/icons/second/${nextWeatherIcon}.png></image>
  </div>
  <div class="col-6 mr-2 no-padding">
  <h5 class = "max-temp"> Max ${maxTemp}${unitsSign}</h5>
  <h5 class = "min-temp"> Min ${minTemp}${unitsSign}</h5>
  </div>
  </div>
  <h8>${nextWeatherMain}</h8>
  <div><h2 class = "text-white centered">${dayOfTheWeekString}</h2><h4 class = "text-light centered">Today</h4></div>`);
  return data;
};

const displayForecastForSecondDay = (data) => {
  const unitsSign = database.getCurrentUnitsSign();
  const maxTemp = data.secondMaxTemp;
  const minTemp = data.secondMinTemp;
  const dayOfTheWeekAbreviation = data.secondDayOfTheWeekAbrev;
  const dayOfTheWeekString = dayOfTheWeek(dayOfTheWeekAbreviation);
  const weatherMain = data.secondWeatherMain;
  const weatherIcon = data.secondWeatherIcon;
  $('#secondDay').css('background-image', `url("./src/images/backgrounds/${weatherIcon}e.png")`).css('background-size', 'cover')
      .addClass('forecast-border')
      .html(`
  <div class="row mb-3 margin-top-left">
  <div class="col no-padding">
  <image src = ./src/images/icons/second/${weatherIcon}.png></image>
  </div>
  <div class="col-6 mr-2 no-padding">
  <h5 class = "max-temp"> Max ${maxTemp}${unitsSign}</h5>
  <h5 class = "min-temp"> Min ${minTemp}${unitsSign}</h5>
  </div>
  </div>
  <h8>${weatherMain}</h8>
  <h2 class = "text-white centered">${dayOfTheWeekString}</h2>`);
  return data;
};

const displayForecastForThirdDay = (data) => {
  const unitsSign = database.getCurrentUnitsSign();
  const maxTemp = data.thirdMaxTemp;
  const minTemp = data.thirdMinTemp;
  const dayOfTheWeekAbreviation = data.thirdDayOfTheWeekAbrev;
  const dayOfTheWeekString = dayOfTheWeek(dayOfTheWeekAbreviation);
  const weatherMain = data.thirdWeatherMain;
  const weatherIcon = data.thirdWeatherIcon;
  $('#thirdDay').css('background-image', `url("./src/images/backgrounds/${weatherIcon}e.png")`).css('background-size', 'cover')
      .addClass('forecast-border')
      .html(`
  <div class="row mb-3 margin-top-left">
  <div class="col no-padding">
  <image src = ./src/images/icons/second/${weatherIcon}.png></image>
  </div>
  <div class="col mr-2  no-padding">
  <h5 class = "max-temp"> Max ${maxTemp}${unitsSign}</h5>
  <h5 class = "min-temp"> Min ${minTemp}${unitsSign}</h5>
  </div>
  </div>
  <h8>${weatherMain}</h8>
  <h2 class = "text-white centered">${dayOfTheWeekString}</h2>`);
  return data;
};

const displayForecastForFourthDay = (data) => {
  const unitsSign = database.getCurrentUnitsSign();
  const maxTemp = data.fourthMaxTemp;
  const minTemp = data.fourthMinTemp;
  const dayOfTheWeekAbreviation = data.fourthDayOfTheWeekAbrev;
  const dayOfTheWeekString = dayOfTheWeek(dayOfTheWeekAbreviation);
  const weatherMain = data.fourthWeatherMain;
  const weatherIcon = data.fourthWeatherIcon;
  $('#fourthDay').css('background-image', `url("./src/images/backgrounds/${weatherIcon}e.png")`).css('background-size', 'cover')
      .addClass('forecast-border')
      .html(`
  <div class="row mb-3 margin-top-left">
  <div class="col no-padding">
  <image src = ./src/images/icons/second/${weatherIcon}.png></image>
  </div>
  <div class="col mr-2  no-padding">
  <h5 class = "max-temp"> Max ${maxTemp}${unitsSign}</h5>
  <h5 class = "min-temp"> Min ${minTemp}${unitsSign}</h5>
  </div>
  </div>
  <h8>${weatherMain}</h8>
  <h2 class = "text-white centered">${dayOfTheWeekString}</h2>`);
  return data;
};

const displayForecastForFifthDay = (data) => {
  const unitsSign = database.getCurrentUnitsSign();
  const maxTemp = data.fifthMaxTemp;
  const minTemp = data.fifthMinTemp;
  const dayOfTheWeekAbreviation = data.fifthDayOfTheWeekAbrev;
  const dayOfTheWeekString = dayOfTheWeek(dayOfTheWeekAbreviation);
  const weatherMain = data.fifthWeatherMain;
  const weatherIcon = data.fifthWeatherIcon;
  $('#fifthDay').css('background-image', `url("./src/images/backgrounds/${weatherIcon}e.png")`).css('background-size', 'cover')
      .addClass('forecast-border')
      .html(`
  <div class="row mb-3 margin-top-left">
  <div class="col no-padding">
  <image src = ./src/images/icons/second/${weatherIcon}.png></image>
  </div>
  <div class="col mr-2  no-padding">
  <h5 class = "max-temp"> Max ${maxTemp}${unitsSign}</h5>
  <h5 class = "min-temp"> Min ${minTemp}${unitsSign}</h5>
  </div>
  </div>
  <h8>${weatherMain}</h8>
  <h2 class = "text-white centered">${dayOfTheWeekString}</h2>`);
  return data;
};

const displayFirstHourlyData = (data) => {
  const unitsSign = database.getCurrentUnitsSign();
  const currentHour = forecastData.getHourlyData(data, 0).currentHour;
  const hourlyTemperature = forecastData.getHourlyData(data, 0).hourlyTemperature;
  const hourIcon = forecastData.getHourlyData(data, 0).hourIcon;
  const hourWeatherDescription = forecastData.getHourlyData(data, 0).hourWeatherDescription;
  $(firstHourGroup).html(`
  <div class="text-center">
  <div>
  <h1>${hourlyTemperature}${unitsSign}</h1>
  </div>
  <div class="col no-padding">
  <image src = ./src/images/icons/second/${hourIcon}.png></image>
  </div>
  <div class="mb-3">
  <h8>${hourWeatherDescription}</h8>
  </div>
  <h4 class="mb-3">${currentHour}</h4>
  </div>`).css('background-image', `url("./src/images/backgrounds/${hourIcon}e.png")`).css('background-size', 'cover');
  $('#hourlyForecast').addClass('forecast-border');

  return data;
};

const displaySecondHourlyData = (data) => {
  const unitsSign = database.getCurrentUnitsSign();
  const currentHour = forecastData.getHourlyData(data, 1).currentHour;
  const hourlyTemperature = forecastData.getHourlyData(data, 1).hourlyTemperature;
  const hourIcon = forecastData.getHourlyData(data, 1).hourIcon;
  const hourWeatherDescription = forecastData.getHourlyData(data, 1).hourWeatherDescription;
  $(secondHourGroup).html(`
  <div class="text-center">
  <div>
  <h1>${hourlyTemperature}${unitsSign}</h1>
  </div>
  <div class="col no-padding">
  <image src = ./src/images/icons/second/${hourIcon}.png></image>
  </div>
  <div class="mb-3">
  <h8>${hourWeatherDescription}</h8>
  </div>
  <h4 class="mb-3">${currentHour}</h4>
  </div>`).css('background-image', `url("./src/images/backgrounds/${hourIcon}e.png")`).css('background-size', 'cover');

  return data;
};

const displayThirdHourlyData = (data) => {
  const unitsSign = database.getCurrentUnitsSign();
  const currentHour = forecastData.getHourlyData(data, 2).currentHour;
  const hourlyTemperature = forecastData.getHourlyData(data, 2).hourlyTemperature;
  const hourIcon = forecastData.getHourlyData(data, 2).hourIcon;
  const hourWeatherDescription = forecastData.getHourlyData(data, 2).hourWeatherDescription;
  $(thirdHourGroup).html(`
  <div class="text-center">
  <div>
  <h1>${hourlyTemperature}${unitsSign}</h1>
  </div>
  <div class="col no-padding">
  <image src = ./src/images/icons/second/${hourIcon}.png></image>
  </div>
  <div class="mb-3">
  <h8>${hourWeatherDescription}</h8>
  </div>
  <h4 class="mb-3">${currentHour}</h4>
  </div>`).css('background-image', `url("./src/images/backgrounds/${hourIcon}e.png")`).css('background-size', 'cover');

  return data;
};

const displayFourthHourlyData = (data) => {
  const unitsSign = database.getCurrentUnitsSign();
  const currentHour = forecastData.getHourlyData(data, 3).currentHour;
  const hourlyTemperature = forecastData.getHourlyData(data, 3).hourlyTemperature;
  const hourIcon = forecastData.getHourlyData(data, 3).hourIcon;
  const hourWeatherDescription = forecastData.getHourlyData(data, 3).hourWeatherDescription;
  $(fourthHourGroup).html(`
  <div class="text-center">
  <div>
  <h1>${hourlyTemperature}${unitsSign}</h1>
  </div>
  <div class="col no-padding">
  <image src = ./src/images/icons/second/${hourIcon}.png></image>
  </div>
  <div class="mb-3">
  <h8>${hourWeatherDescription}</h8>
  </div>
  <h4 class="mb-3">${currentHour}</h4>
  </div>`).css('background-image', `url("./src/images/backgrounds/${hourIcon}e.png")`).css('background-size', 'cover');

  return data;
};

const displayFifthHourlyData = (data) => {
  const unitsSign = database.getCurrentUnitsSign();
  const currentHour = forecastData.getHourlyData(data, 4).currentHour;
  const hourlyTemperature = forecastData.getHourlyData(data, 4).hourlyTemperature;
  const hourIcon = forecastData.getHourlyData(data, 4).hourIcon;
  const hourWeatherDescription = forecastData.getHourlyData(data, 4).hourWeatherDescription;
  $(fifthHourGroup).html(`
  <div class="text-center">
  <div>
  <h1>${hourlyTemperature}${unitsSign}</h1>
  </div>
  <div class="col no-padding">
  <image src = ./src/images/icons/second/${hourIcon}.png></image>
  </div>
  <div class="mb-3">
  <h8>${hourWeatherDescription}</h8>
  </div>
  <h4 class="mb-3">${currentHour}</h4>
  </div>`).css('background-image', `url("./src/images/backgrounds/${hourIcon}e.png")`).css('background-size', 'cover');

  return data;
};

const displaySixthHourlyData = (data) => {
  const unitsSign = database.getCurrentUnitsSign();
  const currentHour = forecastData.getHourlyData(data, 5).currentHour;
  const hourlyTemperature = forecastData.getHourlyData(data, 5).hourlyTemperature;
  const hourIcon = forecastData.getHourlyData(data, 5).hourIcon;
  const hourWeatherDescription = forecastData.getHourlyData(data, 5).hourWeatherDescription;
  $(sixthHourGroup).html(`
  <div class="text-center">
  <div>
  <h1>${hourlyTemperature}${unitsSign}</h1>
  </div>
  <div class="col no-padding">
  <image src = ./src/images/icons/second/${hourIcon}.png></image>
  </div>
  <div class="mb-3">
  <h8>${hourWeatherDescription}</h8>
  </div>
  <h4 class="mb-3">${currentHour}</h4>
  </div>`).css('background-image', `url("./src/images/backgrounds/${hourIcon}e.png")`).css('background-size', 'cover');

  return data;
};

const displaySeventhHourlyData = (data) => {
  const unitsSign = database.getCurrentUnitsSign();
  const currentHour = forecastData.getHourlyData(data, 6).currentHour;
  const hourlyTemperature = forecastData.getHourlyData(data, 6).hourlyTemperature;
  const hourIcon = forecastData.getHourlyData(data, 6).hourIcon;
  const hourWeatherDescription = forecastData.getHourlyData(data, 6).hourWeatherDescription;
  $(seventhHourGroup).html(`
  <div class="text-center">
  <div>
  <h1>${hourlyTemperature}${unitsSign}</h1>
  </div>
  <div class="col no-padding">
  <image src = ./src/images/icons/second/${hourIcon}.png></image>
  </div>
  <div class="mb-3">
  <h8>${hourWeatherDescription}</h8>
  </div>
  <h4 class="mb-3">${currentHour}</h4>
  </div>`).css('background-image', `url("./src/images/backgrounds/${hourIcon}e.png")`).css('background-size', 'cover');

  return data;
};

const displayEigthHourlyData = (data) => {
  const unitsSign = database.getCurrentUnitsSign();
  const currentHour = forecastData.getHourlyData(data, 7).currentHour;
  const hourlyTemperature = forecastData.getHourlyData(data, 7).hourlyTemperature;
  const hourIcon = forecastData.getHourlyData(data, 7).hourIcon;
  const hourWeatherDescription = forecastData.getHourlyData(data, 7).hourWeatherDescription;
  $(eigthHourGroup).html(`
  <div class="text-center">
  <div>
  <h1>${hourlyTemperature}${unitsSign}</h1>
  </div>
  <div class="col no-padding">
  <image src = ./src/images/icons/second/${hourIcon}.png></image>
  </div>
  <div class="mb-3">
  <h8>${hourWeatherDescription}</h8>
  </div>
  <h4 class="mb-3">${currentHour}</h4>
  </div>`).css('background-image', `url("./src/images/backgrounds/${hourIcon}e.png")`).css('background-size', 'cover');

  return data;
};

const render = (cityName) => {
  database.getWeatherByName(cityName)
      .then(displayFirstHourlyData)
      .then(displaySecondHourlyData)
      .then(displayThirdHourlyData)
      .then(displayFourthHourlyData)
      .then(displayFifthHourlyData)
      .then(displaySixthHourlyData)
      .then(displaySeventhHourlyData)
      .then(displayEigthHourlyData)
      .then(forecastData.getForecastData)
      .then(displayForecastForFirstDay)
      .then(displayForecastForSecondDay)
      .then(displayForecastForThirdDay)
      .then(displayForecastForFourthDay)
      .then(displayForecastForFifthDay);
};

const renderCurrentLocation = () => {
  database.getCurrentPosition();
  database.getCurrentLocationForecast()
      .then(displayFirstHourlyData)
      .then(displaySecondHourlyData)
      .then(displayThirdHourlyData)
      .then(displayFourthHourlyData)
      .then(displayFifthHourlyData)
      .then(displaySixthHourlyData)
      .then(displaySeventhHourlyData)
      .then(displayEigthHourlyData)
      .then(forecastData.getForecastData)
      .then(displayForecastForFirstDay)
      .then(displayForecastForSecondDay)
      .then(displayForecastForThirdDay)
      .then(displayForecastForFourthDay)
      .then(displayForecastForFifthDay);
};

export const forecast = {
  render,
  renderCurrentLocation,
};
