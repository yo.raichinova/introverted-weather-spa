import {
	database,
} from './helpers/database.js';

const convertDegrees = (data) => {
	const tempConversion = [(1.8 * data.main.temp + 32).toFixed(0) + '°F', Math.round(data.main.temp) + '°C'];

	return data;
};

const render = (cityName) => {
	database.getCurrentWeatherByName(cityName)
		.then(convertDegrees);
};

export const degreesConverter = {
	render,
};


/* 
$(".temp-celsius").html(tempConversion[0]);
	$(".temp-fahrenheit").html(tempConversion[1]);
	$(".temperature").click(function () {
		$(".temp-celsius").toggle();
		$(".temp-fahrenheit").toggle();
	}); */