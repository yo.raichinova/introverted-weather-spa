const render = () => {
  $('.current-weather').html(`
    <h1 class="text-secondary">Welcome to the <span class="text-white">Introverted Weather</span>!</h1>
    <h4 class="text-secondary">Search for a city location or pick from one of the following:</h4>`);
  $('.current-weather').removeClass('forecast-border');
  $('.current-weather').removeAttr('style')
  $('#daily').empty();
  $('#hourly').empty();
  $('#firstDay').empty().removeClass('forecast-border');
  $('#secondDay').empty().removeClass('forecast-border');
  $('#thirdDay').empty().removeClass('forecast-border');
  $('#fourthDay').empty().removeClass('forecast-border');
  $('#fifthDay').empty().removeClass('forecast-border');
  $('#hourlyForecast').removeClass('forecast-border');
  $('#firstHourGroup').empty();
  $('#secondHourGroup').empty();
  $('#thirdHourGroup').empty();
  $('#fourthHourGroup').empty();
  $('#fifthHourGroup').empty();
  $('#sixthHourGroup').empty();
  $('#seventhHourGroup').empty();
  $('#eigthHourGroup').empty();
  $('body').removeClass('bg-color-day');
  $('body').removeClass('bg-color-night');
};

export const homePage = {
  render,
};
